addFilter = function(filtertype) {
  if (sessionStorage.getItem(filtertype) != 'true') {
    sessionStorage.setItem(filtertype,'true');

    var table = document.getElementById("table-filter");
    rowcounter = $('table#table-filter tr:last').index();
    var row = table.insertRow(rowcounter+1);
    var cellcounter = 0;
    row.insertCell(cellcounter);
    cellcounter++;

    //add Delete Button;
    var htmlDeleteButton = [];
    var cellDelete = row.insertCell(cellcounter);
    cellcounter++;

    //add Title to cell;
    var cellTitle = row.insertCell(cellcounter);
    cellcounter++;

    //add different fields for each case of filter
    if (filtertype == 'publishedDate') {
      cellTitle.innerHTML = "Published Year: ";
      $('#liPublishedDate').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm" onClick="deleteRow(this,0)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var cellPDateFrom = row.insertCell(cellcounter);
      cellcounter++;
      var htmlPDateFrom = [];
      htmlPDateFrom.push('<form id="filter-pubdateMin" class=""><div class="input-group input-group-sm"><span class="input-group-addon" style="min-width:66px" id="input-PDMin">from</span><input onClick="this.select();" id="searchPublishedDateMin" type="text" class="form-control" placeholder="..." aria-describedby="sizing-addon3"></div></form>');
      cellPDateFrom.innerHTML = htmlPDateFrom;

      var cellPDateTo = row.insertCell(cellcounter);
      cellcounter++;
      var htmlPDateTo = [];
      htmlPDateTo.push('<form id="filter-pubdateMax" class=""><div class="input-group input-group-sm"><span class="input-group-addon" style="min-width:66px" id="input-PDMax">to</span><input onClick="this.select();" id="searchPublishedDateMax" type="text" class="form-control" placeholder="..." aria-describedby="sizing-addon3"></div></form>');
      cellPDateTo.innerHTML = htmlPDateTo;

      var cellPDateNA = row.insertCell(cellcounter);
      cellcounter++;
      var htmlPDateNA = [];
      htmlPDateNA.push('<div class="material-switch"><input id="someSwitchOptionPrimary" name="someSwitchOption001" checked="checked" type="checkbox"/><label for="someSwitchOptionPrimary" class="label-primary"></label><span id="spanPDUndefinied">Include undefinied dates</span></div>');
      cellPDateNA.innerHTML = htmlPDateNA;
    }
    else if (filtertype == 'author') {
      cellTitle.innerHTML = "Author: ";
      $('#liAuthor').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm" onClick="deleteRow(this,1)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var cellAuthorContains = row.insertCell(cellcounter);
      cellcounter++;
      var htmlAuthorContains = [];
      htmlAuthorContains.push('<form id="filter-author" class=""><div class="input-group input-group-sm "><span class="input-group-addon" id="sizing-addon3">contains</span><input onClick="this.select();" id="searchAuthorContains" type="text" class="form-control" placeholder="..." aria-describedby="sizing-addon3"></div></form>');
      cellAuthorContains.innerHTML = htmlAuthorContains;
    }
    else if (filtertype == 'title') {
      cellTitle.innerHTML = "Title: ";
      $('#liTitle').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm" onClick="deleteRow(this,2)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var cellTitleContains = row.insertCell(cellcounter);
      cellcounter++;
      var htmlTitleContains = [];
      htmlTitleContains.push('<form id="filter-title" class=""><div class="input-group input-group-sm"><span class="input-group-addon" id="sizing-addon3">contains</span><input onClick="this.select();" id="searchTitleContains" type="text" class="form-control" placeholder="..." aria-describedby="sizing-addon3"></div></form>');
      cellTitleContains.innerHTML = htmlTitleContains;
    }
    else if (filtertype == 'printType') {
      cellTitle.innerHTML = "Print type: ";
      $('#liPrintType').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm btn" onClick="deleteRow(this,3)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var cellPrintType = row.insertCell(cellcounter);
      cellcounter++;
      var htmlPrintType = [];
      htmlPrintType.push('<select id="searchPrintType" class="selectpicker"><option>All</option><option>All Books</option><option>All Magazines</option></select>');
      cellPrintType.innerHTML = htmlPrintType;

      var cellPlaceholder = row.insertCell(cellcounter);
      cellcounter++;
    }
    else if (filtertype == 'langRestrict') {
      cellTitle.innerHTML = "Restrict Language: ";
      $('#lilangRestrict').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm btn" onClick="deleteRow(this,4)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var celllangRestrict = row.insertCell(cellcounter);
      cellcounter++;
      var htmllangRestrict = [];
      htmllangRestrict.push('<select id="searchlangRestrict" class="selectpicker"><option>All</option><option>English</option><option>German</option><option>French</option></select>');
      celllangRestrict.innerHTML = htmllangRestrict;

      var cellPlaceholder = row.insertCell(cellcounter);
      cellcounter++;
    }
    else if (filtertype == 'textAvailability') {
      cellTitle.innerHTML = "Text Availability: ";
      $('#litextAvailability').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm btn" onClick="deleteRow(this,5)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var celltextAvailability = row.insertCell(cellcounter);
      cellcounter++;
      var htmltextAvailability = [];
      htmltextAvailability.push('<select id="searchtextAvailability" class="selectpicker"><option>All</option><option>Public full-text</option><option>Sample preview</option><option>Under license</option></select>');
      celltextAvailability.innerHTML = htmltextAvailability;

      var cellPlaceholder = row.insertCell(cellcounter);
      cellcounter++;
    }
    else if (filtertype == 'sources') {
      cellTitle.innerHTML = "Select Sources: ";
      $('#liSources').addClass('disabled');

      htmlDeleteButton.push('<a class="btn btn-default btn-sm btn" onClick="deleteRow(this,6)" title="delete this filter"><i class="fa fa-trash-o fa-lg"></i> Delete Filter</a>');
      cellDelete.innerHTML = htmlDeleteButton;

      var cellSourcesGoogle = row.insertCell(cellcounter);
      cellSourcesGoogle.id = 'table-filter-source';
      cellcounter++;
      var htmlSourcesGoogle = [];
      htmlSourcesGoogle.push('<div class="checkbox"><label style="height:20px;"><input id="sourcesGoogle" type="checkbox" value="google" checked>Google Books</label></div>');
      cellSourcesGoogle.innerHTML = htmlSourcesGoogle;

      var cellSourcesGBV = row.insertCell(cellcounter);
      cellSourcesGBV.id = 'table-filter-source';
      cellcounter++;
      var htmlSourcesGBV = [];
      htmlSourcesGBV.push('<div class="checkbox"><label style="height:20px;"><input id="sourcesGBV" type="checkbox" value="gbv" checked>GBV</label></div>');
      cellSourcesGBV.innerHTML = htmlSourcesGBV;

      var cellSourcesTextGrid = row.insertCell(cellcounter);
      cellSourcesTextGrid.id = 'table-filter-source';
      cellcounter++;
      var htmlSourcesTextGrid = [];
      htmlSourcesTextGrid.push('<div class="checkbox"><label style="height:20px;"><input id="sourcesTextGrid" type="checkbox" value="textgrid" checked>TextGrid</label></div>');
      cellSourcesTextGrid.innerHTML = htmlSourcesTextGrid;

      var cellPlaceholder = row.insertCell(cellcounter);
      cellcounter++;
    }
    else {
      cellTitle.innerHTML = "New Row";
    }
  }
  else {
    console.log('error: filtertype already set');
  }

}

deleteRow = function(row,type){
    var i=row.parentNode.parentNode.rowIndex;
    document.getElementById('table-filter').deleteRow(i);
    if (type == 0) {
      $('#liPublishedDate').removeClass('disabled');
      sessionStorage.setItem('publishedDate','false');
    }
    else if (type == 1) {
      $('#liAuthor').removeClass('disabled');
      sessionStorage.setItem('author','false');
    }
    else if (type == 2) {
      $('#liTitle').removeClass('disabled');
      sessionStorage.setItem('title','false');
    }
    else if (type == 3) {
      $('#liPrintType').removeClass('disabled');
      sessionStorage.setItem('printType','false');
    }
    else if (type == 4) {
      $('#lilangRestrict').removeClass('disabled');
      sessionStorage.setItem('langRestrict','false');
    }
    else if (type == 5) {
      $('#litextAvailability').removeClass('disabled');
      sessionStorage.setItem('textAvailability','false');
    }
    else if (type == 6) {
      $('#liSources').removeClass('disabled');
      sessionStorage.setItem('sources','false');
    }
}

checkForFilterErrors = function(){
   keyword=false;
   pubdateMin=true;
   pubdateMax=true;
   author=true;
   title=true;
  //empty keyword
  if($('input#searchText').val().length == 0 && $('form#adv-search2.has-error').size()==0){
    $('form#adv-search2').attr('class', 'has-error');
    $('form#adv-search2').append('<span class="help-block">Please enter a keyword</span>');
    $('div#adv-search').attr('style', 'padding-top:7px;');
    keyword = false;
  }
  else{
      $('form#adv-search2').removeClass('has-error');
      if($('form#adv-search2').children().length == 2){
        $('form#adv-search2').children().last().remove();

      }
    keyword = true;
  }

  //only numbers in publishedDateMin
  if($('input#searchPublishedDateMin').length > 0){
    if(isNaN($('input#searchPublishedDateMin').val())){
      $('form#filter-pubdateMin').attr("class", "has-error");
      $('form#filter-pubdateMin').append('<span class="help-block">Just numbers allowed</span>');
      pubdateMin = false;
    }
    else{
      $('form#filter-pubdateMin').removeClass('has-error');
      if($('form#filter-pubdateMin').children().length == 2){
        $('form#filter-pubdateMin').children().last().remove();
      }
      pubdateMin = true;
    }
  }

  //only numbers in publishedDateMax
  if($('input#searchPublishedDateMax').length > 0){
    if(isNaN($('input#searchPublishedDateMax').val())){
      $('form#filter-pubdateMax').attr("class", "has-error");
      $('form#filter-pubdateMax').append('<span class="help-block">Just numbers allowed</span>');
      pubdateMax = false;
    }
    else{
      $('form#filter-pubdateMax').removeClass('has-error');
      if($('form#filter-pubdateMax').children().length == 2){
        $('form#filter-pubdateMax').children().last().remove();
      }
      pubdateMax = true;
    }
  }
  //only strings for author
  if($('input#searchAuthorContains').length > 0){
    if(!isNaN($('input#searchAuthorContains').val())){
      $('form#filter-author').attr("class", "has-error");
      $('form#filter-author').append('<span class="help-block">No numbers for authers allowed</span>');
      author = false;
    }
    else{
      $('form#filter-author').removeClass('has-error');
      if($('form#filter-author').children().length == 2){
        $('form#filter-author').children().last().remove();
      }
      author = true;
    }
  }

  //only strings for title
  /*if($('input#searchTitleContains').length > 0){
    if(!isNaN($('input#searchTitleContains').val())){
      $('form#filter-title').attr("class", "has-error");
      $('form#filter-title').append('<span class="help-block">Just numbers allowed</span>');
      title = false;
    }
    else{
      $('form#filter-title').removeClass('has-error');
      if($('form#filter-title').children().length == 2){
        $('form#filter-title').children().last().remove();
      }
      title = true;
    }
  }*/


  if(keyword == true && pubdateMin == true && pubdateMax == true && author == true){
    return true;
  }
  else{
    return false;
  }
}

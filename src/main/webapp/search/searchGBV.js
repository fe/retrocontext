var searchGBV;
var jsonGBV = "";
var counterGBV, methodCounter;
var jsondataBeginGBV, jsondataEndGBV, jsondataGBV;
var urls;
var filterTAGBV = "";
var filterPDGBV, searchUndefiniedGBV;
var minDateGBV, maxDateGBV;
var filterAuthor, filterAuthorContains;
var filterTitle, filterTitleContains;
/*var methods = ["gei_digital","digi_sammlungen_uniweimar","digi_bib_meck_pomm","ub_kiel_digital","orka_kassel","vzg-easydb","digishelf","rijksmuseum","viamus","museen-thueringen","ddb","museen-nord","kenom_portal","kulturerbe_niedersachsen","wissenschaftliche-sammlungen-greifswald","sammlung-kirchhoff","ornamentstichsammlung"];
*/var methods = ["kulturerbe_niedersachsen","vzg-easydb","digi_bib_meck_pomm","gei_digital","digi_sammlungen_uniweimar","rijksmuseum","ub_kiel_digital","digishelf","viamus","ddb","museen-nord","kenom_portal","kulturerbe_niedersachsen","wissenschaftliche-sammlungen-greifswald","sammlung-kirchhoff"];
/* Diese Quellen sind böse:orga_kassel, museen-thueringen, ornamentstichsammlung*/

function searchGBV(keyword) {
  filterPDGBV=false;
  searchUndefiniedGBV=true;
  filterAuthor=false;
  filterTitle=false;
  filterTAGBV = "";
  if (sessionStorage.getItem('textAvailability') == 'true') {
    var textAvailabilityVal = $("select#searchtextAvailability").val();
    if (textAvailabilityVal == 'Sample preview') {
      filterTAGBV = "SAMPLE";
    }
    else if (textAvailabilityVal == 'Under license') {
      filterTAGBV = "license";
    }
  }
  if (sessionStorage.getItem('title') == 'true') {
    filterTitle=true;
    filterTitleContains = $("input#searchTitleContains").val();
  }
  if (sessionStorage.getItem('author') == 'true') {
    filterAuthor=true;
    filterAuthorContains = $("input#searchAuthorContains").val();
  }
  if (sessionStorage.getItem('publishedDate') == 'true') {
    filterPDGBV = true;
    if (document.getElementById('someSwitchOptionPrimary').checked == false ) {
      searchUndefiniedGBV = false;
    }
    minDateGBV = parseInt($("input#searchPublishedDateMin").val());
    maxDateGBV = parseInt($("input#searchPublishedDateMax").val());
  }

  urls="";
  jsondataBeginGBV = "[";
  jsondataEndGBV = "]";
  jsondataGBV = "";
  counterGBV = 0;
  methodCounter = 0;
  console.log("Begin searchGBV with Keyword: " + keyword);

  return new Promise( resolve =>
    ajaxCallGBV(keyword).then( result =>{
      result.forEach(function(oneArrayElement) {
        urls = urls + "\n" + oneArrayElement.url;
        var GBVElementDate = parseDate(oneArrayElement.date);
        if (filterCheck(keyword,oneArrayElement.date,oneArrayElement.person,oneArrayElement.title)) {
          ParsingResultsGBV('0', oneArrayElement.title, oneArrayElement.person, GBVElementDate, 'xxx', getIDofURL(oneArrayElement.url), oneArrayElement.copyright, oneArrayElement.url);
        }
      });
      jsondataGBV = jsondataBeginGBV + jsondataGBV + jsondataEndGBV;
      //console.log(jsondataGBV);
      //console.log(urls);
      return resolve(jsondataGBV);
    })
  );
}

function ajaxCallGBV(keyword) {
  //console.log("http://kulturgetriebe.gbv.de/widgetConnector/?method="+methods[methodCounter]+"&search="+keyword);
  return new Promise( resolve => {
    $.ajax({
      type: "GET",
      datatype: "json",
      //For local testing: /kulturgetriebe/ durch http://kulturgetriebe.gbv.de/ ersetzen
      url: "/kulturgetriebe/widgetConnector/?method="+methods[methodCounter]+"&search="+keyword,/*+"&count=11",*/
      cache: false,
      error: function (err) {
          errorAjax(JSON.stringify(err),'GBV');
      },
    }).done(function(data) {
        if(methodCounter < (methods.length-1)) {
          methodCounter++;
          ajaxCallGBV(keyword).then( result => {
            resarr = result.concat(JSON.parse(data));
            resolve(resarr);
          })
        } else {
            //Abbruchbedingung
            var res = JSON.parse(data);
            resolve(res);
        }
      })
  });
};

function getIDofURL(url){
  var id = -1;
  var start = url.indexOf("objekt");
  var value = "objekt";
  var length = value.length+1;
  if (start == -1) {
    start = url.indexOf("object");
    value = "object";
    length = value.length+1;
  }
  if (start == -1) {
    start = url.indexOf("Objekt");
    value = "Object";
    length = value.length+1;
  }
  if (start == -1) {
    start = url.indexOf("image");
    value = "image";
    length = value.length+1;
  }
  if (start == -1) {
    start = url.indexOf("collection");
    value = "collection";
    length = value.length+1;
  }
  if (start != -1) {
    start=start+length;
    var urltemp = url.substring(start);
    var end = urltemp.indexOf("/");
    if (end == -1) {
      end = urltemp.length;
    }
    id = urltemp.substring(0,end);
  }
  return id;
}

function ParsingResultsGBV(id, title, author, publishedDate, textAvailability, sourceid, sourceGBV, url) {
  if (filterTAGBV == '') {
    var newtitle="";
    var newauthor="";
    sourceGBV = sourceGBV + " (GBV)";

    if ((title != "") && (typeof title !== "undefined")){
      //console.log("Title: " + title);
      newtitle = title.replace(/[\"]/g, "'");
      //console.log("New Title: " + newtitle);
      //var titleParts = title.split(" ");
      /*for (_i = 0, _len = titleParts.length; _i < _len; _i++){
        titlePart = titleParts[_i];
        titlePart = titlePart.replace('"', "") + " ";
        newtitle = newtitle + titlePart;
      }*/
    }
    else {
      newtitle = "undefinied";
    }

    if ((author != "") && (typeof author !== "undefined")){
     newauthor = author.replace(/[\"]/g, "'");
     /*var authorParts = author.split(" ");
      for (_i = 0, _len = authorParts.length; _i < _len; _i++){
        authorPart = authorParts[_i];
        authorPart = authorPart.replace(/[\"]/g, "'") + " ";
        newauthor = newauthor + authorPart;
      }*/
    }
    else {
      newauthor = "undefinied";
    }

    if (counterGBV == 0) {
      jsondataGBV = "{" +
        "\"id\":" + counterGBV + "," +
        "\"title\":" + "\"" + newtitle + "\"" +  "," +
        "\"author\":" + "\"" + newauthor + "\"" + "," +
        "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
        "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
        "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
        "\"source\":" + "\"" + sourceGBV + "\" " + "," +
        "\"url\":" + "\"" + url + "\" " +
        "}";
    }
    else {
      jsondataTempGBV = ",{" +
        "\"id\":" + counterGBV + "," +
        "\"title\":" + "\"" + newtitle + "\"" +  "," +
        "\"author\":" + "\"" + newauthor + "\"" + "," +
        "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
        "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
        "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
        "\"source\":" + "\"" + sourceGBV + "\" " + "," +
        "\"url\":" + "\"" + url + "\" " +
        "}";
      jsondataGBV = jsondataGBV + jsondataTempGBV;
    }
    counterGBV++;
  }
}

function filterDate(datestring) {
  /*wenn die Datumsangabe lautet: 1900-1999
  und min=1950 und max=1960
  dann wird false zurückgegeben*/
  arrayDates = findDateInString(datestring);
  if ((arrayDates.length == 0) && searchUndefiniedGBV){
    return true;
  }
  else if ((datestring == "n.d.") && searchUndefiniedGBV){
    return true;
  }
  else {
    var min=minDateGBV;
    var max=maxDateGBV;
    for (var i=0;i<=arrayDates.length;i++) {
      if ((arrayDates[i] >= min) && (arrayDates[i] <= max)) {
        return true;
      }
    }
  }
  return false;
}

function findDateInString(datestring) {
  var datestringResult=[];
  try {
    datestringResult = datestring.split(/[^\d]/).filter(function(n){if((n>=1000)&&(n<=2099))return n});
  }
  catch (err) {
    console.log("error: unable to findDateInString for string:" +datestring);
  }
  return datestringResult;
}

function parseDate(datestring) {
  if (datestring == "") {
    datestring = "n.d.";
  }
  else {
    var datestringLength = 0;
    try {
      datestringLength = findDateInString(datestring).length;
    }
    catch (err) {
      console.log("error: unable to detect length for string");
    }
    if (datestringLength = 0) {
      datestring = "n.d. (changed)";
    }
  }
  return datestring;
}

function filterCheck(keyword,date,author,title){
  var returnvalue=false;
  var datecheck=true;
  var titlecheck=true;
  var authorcheck=true;

  if (filterPDGBV) {
    if (filterDate(date)){
        datecheck=true;
    }
    else {datecheck=false;}
  }

  if (filterAuthor){
    if (author.includes(filterAuthorContains)){
      authorcheck=true;
    }
    else {authorcheck=false;}
  }

  if (filterTitle){
    if (title.includes(filterTitleContains)){
      titlecheck=true;
    }
    else {titlecheck=false;}
  }

  if (datecheck && titlecheck && authorcheck) {
    returnvalue=true;
  }

  return returnvalue;
}

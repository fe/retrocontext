var searchGoogle;
var filterAddon = "";
var filterPD = false;
var searchUndefinied = 'true';

searchWiki = function(keyword) {
  filterAddon = "";
  filterPD = false;
  searchUndefinied = 'true';
  console.log("Begin searchGoogle with Keyword: " + keyword);
  if (sessionStorage.getItem('publishedDate') == 'true') {
    filterPD = true;
    if (document.getElementById('someSwitchOptionPrimary').checked == false ) {
      searchUndefinied = 'false';
    }
  }
  if (sessionStorage.getItem('title') == 'true') {
    filterAddon = filterAddon + "+intitle:" + $("input#searchTitleContains").val();
  }
  if (sessionStorage.getItem('author') == 'true') {
    filterAddon = filterAddon + "+inauthor:" + $("input#searchAuthorContains").val();
  }
  if (sessionStorage.getItem('printType') == 'true') {
    var printTypeVal = $("select#searchPrintType").val();
    if (printTypeVal == 'All Magazines') {
      printTypeVal = 'magazines';
    }
    else if (printTypeVal == 'All Books') {
      printTypeVal = 'books';
    }
    else if (printTypeVal == 'All') {
      printTypeVal = 'all';
    }
    filterAddon = filterAddon + "&printType="+printTypeVal;
  }
  console.log('with filters: '+filterAddon);

  promise = $.Deferred();
  $("div#googleBooksResultList").modal("show");
  $("div#googleBooksResultList-results").empty();
  if (filterPD == true) {
    minPublishedDate = parseInt($("input#searchPublishedDateMin").val());
    maxPublishedDate = parseInt($("input#searchPublishedDateMax").val());
  }
  else {
    minPublishedDate = 0;
    maxPublishedDate = 2017;
  }
  var jsondata="";
  var resultsCounter=0;
  var iteration = 0;

  function parseResultElement(id, title, author, publishedDate, textAvailability, sourceid) {
    if (publishedDate == "0") {
      publishedDate = "n.d.";
    }
    /*
    if (textAvailability == 'NOT_FOR_SALE') {
      textAvailability = "NOT_FOR_SALE";
    }
    else {
      textAvailability = "other";
    }
    */
    if (resultsCounter == 0) {
      jsondata = "{" +
        "\"id\":" + resultsCounter + "," +
        "\"title\":" + "\"" + title + "\"" +  "," +
        "\"author\":" + "\"" + author + "\"" + "," +
        "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
        "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
        "\"sourceid\":" + "\"" + sourceid + "\" " +
        "}";
    }
    else {
      jsondataTemp = ",{" +
        "\"id\":" + resultsCounter + "," +
        "\"title\":" + "\"" + title + "\"" +  "," +
        "\"author\":" + "\"" + author + "\"" + "," +
        "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
        "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
        "\"sourceid\":" + "\"" + sourceid + "\" " +
        "}";
      jsondata = jsondata + jsondataTemp;
    }
    resultsCounter++;
  }

  //Alle Google Ergebnisse abholen, then: Parsing der Ergebnisse
  ajaxCallWiki(keyword).then( result =>{
    console.log(result);
    ParsingResults(result);

		//altes Format
    //var jsondataBegin = "{" + "\"total\" : " + resultsCounter + "," + "\"rows\" : [";
    //var jsondataEnd = "]}";
		//neues FOrmat, weil wir jetzt Client-Side-Pagination nutzen:
    var jsondataBegin = "[";
    var jsondataEnd = "]";

    jsondata = jsondataBegin + jsondata + jsondataEnd;
    console.log("JSONDATA");
    console.log(jsondata);
    $("span#numberOfResults").text('count: '+resultsCounter);

    return promise.resolve(jsondata);
  });

  function ajaxCallWiki(keyword) {
    /*var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://commons.wikimedia.org/w/api.php?action=query&format=json&list=search&srsearch=" + keyword + "&srwhat=text&srprop=categorysnippet%7Ctitlesnippet");
    xhr.setRequestHeader( 'Api-User-Agent', 'Example/1.0' );
    xhr.setRequestHeader('Access-Control-Allow-Origin', "*");	
    xhr.send();*/
    $wgCrossSiteAJAXdomains = Array( '*' );
    //xhr.setRequestHeader("Content-Type: application/json", "Authorization: Basic //AuthKey");
    //xhr.send()
    console.log("KEYWORD " + keyword);
    return new Promise( resolve => {
      
      $.ajax({
        type: "GET",
        //jsonp: "callback",
        datatype: "json",
//        data: {q: "asa", format: "json"},
	headers: { 'Api-User-Agent': 'Example/1.0' },
        url: "https://commons.wikimedia.org/w/api.php?action=query&format=json&list=search&srsearch=" + keyword + "&srwhat=text&srprop=categorysnippet%7Ctitlesnippet",      
        cache: false
      }).done(function(data) {
        totalresults = data.totalItems;
        iteration = iteration + 40;
        console.log("DATA FOR WIKI: ");
        console.log(data);
        if(iteration < totalresults) {
          ajaxCallGoogle(keyword).then( result => {
            result.push(data.items);
            resolve(result);
          })
        } else {
            //Abbruchbedingung
            var res = [data.items];
            resolve(res);
        }
      });
    });
  };

  function ParsingResults(resultArray) {
    //Variablen für Parsing
    var title, author, id, publishedDate, selfLink, sourceid, textAvailability, formattedPublishedDate, jsondataTemp;

    resultArray.forEach(function(oneArrayElement) {
      //Gehe durch jedes Ergebnis
      if (typeof oneArrayElement === "undefined") {
        console.log("Problem: first Array = undefined")
      }
      if (typeof oneArrayElement !== "undefined") {
        oneArrayElement.forEach(function(oneResultElement) {
          if (typeof oneResultElement === "undefined") {
            console.log("Problem: first Array of oneResultElement = undefined")
          }
          if (typeof oneResultElement !== "undefined") {
            title = oneResultElement["volumeInfo"]["title"];
            author = oneResultElement["volumeInfo"]["authors"];
            id = oneResultElement["id"];
            textAvailability = oneResultElement["accessInfo"]["accessViewStatus"];
            publishedDate = oneResultElement["volumeInfo"]["publishedDate"];
            //selfLink = oneResultElement["selfLink"];
            sourceid = oneResultElement["id"];
            title = title.replace(/"/g, '\'');
            if (publishedDate !== undefined) {
              formattedPublishedDate = publishedDate.substring(0, 4);
              formattedPublishedDate = parseInt(formattedPublishedDate, 10);
              if ((formattedPublishedDate < maxPublishedDate) && (formattedPublishedDate > minPublishedDate)) {
                parseResultElement(id, title, author, publishedDate, textAvailability, sourceid);
              }
            }
            else {
              publishedDate = "0";
              if (searchUndefinied == 'true' ) {
                parseResultElement(id, title, author, publishedDate, textAvailability, sourceid);
              }
            }
            //Wenn Datum innerhalb Min und Max Published Date => hinzufügen zu jsondata
          }
        });
      }
    });
  }
return promise;
}

var searchGoogle;
var filterAddon = "";
var filterPD = false;
var searchUndefinied = 'true';
var filterTA = "";
var jsondata;
var iteration;
var counterGoogle;

searchGoogle = function(keyword) {
  counterGoogle=0;
  filterAddon = "";
  filterPD = false;
  searchUndefinied = 'true';
  console.log("Begin searchGoogle with Keyword: " + keyword);
  if (sessionStorage.getItem('publishedDate') == 'true') {
    filterPD = true;
    if (document.getElementById('someSwitchOptionPrimary').checked == false ) {
      searchUndefinied = 'false';
    }
  }
  if (sessionStorage.getItem('title') == 'true') {
    filterAddon = filterAddon + "+intitle:" + $("input#searchTitleContains").val();
  }
  if (sessionStorage.getItem('author') == 'true') {
    filterAddon = filterAddon + "+inauthor:" + $("input#searchAuthorContains").val();
  }
  if (sessionStorage.getItem('langRestrict') == 'true') {
    var langRestrictVal = $("select#searchlangRestrict").val();
    if (langRestrictVal == 'English') {
      langRestrictVal = 'en';
      filterAddon = filterAddon + "&langRestrict=" + langRestrictVal;
    }
    else if (langRestrictVal == 'All') {
      /*Do Nothing*/
    }
    else if (langRestrictVal == 'German') {
      langRestrictVal = 'de';
      filterAddon = filterAddon + "&langRestrict=" + langRestrictVal;
    }
    else if (langRestrictVal == 'French') {
      langRestrictVal = 'fr';
      filterAddon = filterAddon + "&langRestrict=" + langRestrictVal;
    }
  }
  if (sessionStorage.getItem('textAvailability') == 'true') {
    var textAvailabilityVal = $("select#searchtextAvailability").val();
    if (textAvailabilityVal == 'Public full-text') {
      filterTA = "FULL_PUBLIC_DOMAIN";
    }
    else if (textAvailabilityVal == 'All') {
      filterTA = "";
    }
    else if (textAvailabilityVal == 'Sample preview') {
      filterTA = "SAMPLE";
    }
    else if (textAvailabilityVal == 'Under license') {
      filterTA = "license";
    }
  }
  if (sessionStorage.getItem('printType') == 'true') {
    var printTypeVal = $("select#searchPrintType").val();
    if (printTypeVal == 'All Magazines') {
      printTypeVal = 'magazines';
    }
    else if (printTypeVal == 'All Books') {
      printTypeVal = 'books';
    }
    else if (printTypeVal == 'All') {
      printTypeVal = 'all';
    }
    filterAddon = filterAddon + "&printType="+printTypeVal;
  }
  console.log('with filters: '+filterAddon);

  $("div#googleBooksResultList").modal("show");
  $("div#googleBooksResultList-results").empty();
  if (filterPD == true) {
    minPublishedDate = parseInt($("input#searchPublishedDateMin").val());
    maxPublishedDate = parseInt($("input#searchPublishedDateMax").val());
  }
  else {
    minPublishedDate = 0;
    maxPublishedDate = 2017;
  }
  jsondata="";
  iteration = 0;

  //Alle Google Ergebnisse abholen, then: Parsing der Ergebnisse
  console.log("Call Google with query: "+"https://www.googleapis.com/books/v1/volumes?q="+keyword+filterAddon+"&orderBy=relevance&maxResults=40&startIndex=1"+"&key=AIzaSyCcCsOBokPZQG5DAZJbQMLAQPrJak7q7E0");
  return new Promise( resolve =>
    ajaxCallGoogle(keyword).then( result =>{
      ParsingResults(result);

      var jsondataBegin = "[";
      var jsondataEnd = "]";

      jsondata = jsondataBegin + jsondata + jsondataEnd;

      return resolve(jsondata);
    })
  );
}

function ajaxCallGoogle(keyword) {
  return new Promise( resolve => {
    $.ajax({
      type: "GET",
      datatype: "json",
      url: "https://www.googleapis.com/books/v1/volumes?q="+keyword+filterAddon+"&orderBy=relevance&maxResults=40&startIndex="+iteration+"&key=AIzaSyCcCsOBokPZQG5DAZJbQMLAQPrJak7q7E0",
      cache: false,
      error: function (err) {
          errorAjax(JSON.stringify(err),'Google');
      },
    }).done(function(data) {
      totalresults = data.totalItems;
      iteration = iteration + 40;

      if(iteration < totalresults) {
        ajaxCallGoogle(keyword).then( result => {
          result.push(data.items);
          resolve(result);
        })
      } else {
          //Abbruchbedingung
          var res = [data.items];
          resolve(res);
      }
    });
  });
};

function ParsingResults(resultArray) {
  //Variablen für Parsing
  var title, author, id, publishedDate, selfLink, sourceid, textAvailability, formattedPublishedDate, jsondataTemp;

  resultArray.forEach(function(oneArrayElement) {
    //Gehe durch jedes Ergebnis
    if (typeof oneArrayElement === "undefined") {
      //console.log("Problem: first Array = undefined")
    }
    if (typeof oneArrayElement !== "undefined") {
      oneArrayElement.forEach(function(oneResultElement) {
        if (typeof oneResultElement === "undefined") {
          console.log("Problem: first Array of oneResultElement = undefined")
        }

        if (typeof oneResultElement !== "undefined") {
          if (typeof oneResultElement["volumeInfo"]["title"] !== "undefined"){
	    title = oneResultElement["volumeInfo"]["title"];
    }

          if (typeof oneResultElement["volumeInfo"]["authors"] !== "undefined"){
            author = oneResultElement["volumeInfo"]["authors"];
	    author = JSON.stringify(author).replace(/[\\"]/g, "");
	    author = author.replace(/[\[\]]/g, "");
	    //author = JSON.stringify(author).replace(/[\]]/g, "");

    }
          id = oneResultElement["id"];
          textAvailability = oneResultElement["accessInfo"]["accessViewStatus"];
          publishedDate = oneResultElement["volumeInfo"]["publishedDate"];
          selfLink = oneResultElement["selfLink"];
          sourceid = oneResultElement["id"];
          title = title.replace(/[\"]/g, "'");

          if (publishedDate !== undefined) {
            formattedPublishedDate = publishedDate.substring(0, 4);
            formattedPublishedDate = parseInt(formattedPublishedDate, 10);
            if ((formattedPublishedDate < maxPublishedDate) && (formattedPublishedDate > minPublishedDate)) {
              checkTA(id, title, author, publishedDate, textAvailability, sourceid, selfLink);
            }
          }
          else {
            publishedDate = "0";
            if (searchUndefinied == 'true' ) {
              checkTA(id, title, author, publishedDate, textAvailability, sourceid, selfLink);
            }
          }
          //Wenn Datum innerhalb Min und Max Published Date => hinzufügen zu jsondata
        }
      });
    }
  });
}

function parseResultElement(id, title, author, publishedDate, textAvailability, sourceid, url) {
  var newtitle="";
  var newauthor="";
  if (publishedDate == "0") {
    publishedDate = "n.d.";
  }
  if ((title != "") && (typeof title !== "undefined")){
    newtitle = title.replace(/[\"]/g, "'");
    /*var titleParts = title.split(" ");
    for (_i = 0, _len = titleParts.length; _i < _len; _i++){
      titlePart = titleParts[_i];
      titlePart = titlePart.replace('"', "") + " ";
      newtitle = newtitle + titlePart;
    }*/
  }
  else {
    newtitle = "undefined";
  }
  if ((author != "") && (typeof author !== "undefined")){
    newauthor = author.replace(/[\"]/g, "'");
  }
  else{
    newauthor = "undefined";
  }

  if (counterGoogle == 0) {
    jsondata = "{" +
      "\"id\":" + counterGoogle + "," +
      "\"title\":" + "\"" + newtitle + "\"" +  "," +
      "\"author\":" + "\"" + newauthor + "\"" + "," +
      "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
      "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
      "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
      "\"source\":" + "\"" + "Google Books" + "\" " + "," +
      "\"url\":" + "\"" + url + "\" " +
      "}";
  }
  else {
    jsondataTemp = ",{" +
      "\"id\":" + counterGoogle + "," +
      "\"title\":" + "\"" + newtitle + "\"" +  "," +
      "\"author\":" + "\"" + newauthor + "\"" + "," +
      "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
      "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
      "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
      "\"source\":" + "\"" + "Google Books" + "\" " + "," +
      "\"url\":" + "\"" + url + "\" " +
      "}";
    jsondata = jsondata + jsondataTemp;
  }
  counterGoogle++;
}

function checkTA(id, title, author, publishedDate, textAvailability, sourceid, url) {
  if (filterTA != '') {
    if (textAvailability == filterTA) {
      parseResultElement(id, title, author, publishedDate, textAvailability, sourceid, url);
    }
  }
  else {
    parseResultElement(id, title, author, publishedDate, textAvailability, sourceid, url);
  }
}

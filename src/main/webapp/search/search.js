var resultsCounter;
var emptyresult=[];

searchFederate = function(searchword) {
  sessionStorage.setItem('error',false);
  resultsCounter=0;
  $("span#numberOfResults").text('count: calculating...');
  $("span#timeForSearch").text('duration: calculating...');
  var checkGoogle=true;
  var checkGBV=true;
  var checkTextGrid=true;

  if (sessionStorage.getItem('sources') == 'true') {
    if (document.getElementById('sourcesGoogle').checked == false) {
      checkGoogle=false;
    }
  }
  if (sessionStorage.getItem('sources') == 'true') {
    if (document.getElementById('sourcesGBV').checked == false) {
      checkGBV=false;
    }
  }
  if (sessionStorage.getItem('sources') == 'true') {
    if (document.getElementById('sourcesTextGrid').checked == false) {
      checkTextGrid=false;
    }
  }

  var pSearchGoogle = new Promise((resolve, reject) => {
    if (checkGoogle) {
      try {
        searchGoogle(searchword).then( result => {
          var JSONdata = [];
          try {
            JSONdata = JSON.parse(result)
          }
          catch(err) {
            console.log("Error for source Google: JSON.parse failed! Following result is not valid:");
            console.log(result);
          }
          return resolve(JSONdata);
        });
      }
      catch(err) {
          errorAjax(err.message,'search.js');
      }
    }
    else {return resolve(emptyresult);}
  });

  var pSearchGBV = new Promise((resolve, reject) => {
    if (checkGBV) {
      try {
        searchGBV(searchword).then( result => {
          var JSONdata = [];
          try {
            JSONdata = JSON.parse(result)
          }
          catch(err) {
            console.log("Error for source GBV: JSON.parse failed! Following result is not valid:");
            console.log(result);
          }
          return resolve(JSONdata);
        });
      }
      catch(err) {
          errorAjax(err.message,'search.js');
      }
    }
    else {return resolve(emptyresult);}
  });

  var pSearchTextGrid = new Promise((resolve, reject) => {
    if (checkTextGrid) {
	    try{
      		searchTextGrid(searchword).then( result => {
            var JSONdata = [];
            try {
              JSONdata = JSON.parse(result)
            }
            catch(err) {
              console.log("Error for source TextGrid: JSON.parse failed! Following result is not valid:");
              console.log(result);
            }
            return resolve(JSONdata);
          });
	    }catch(err) {
        errorAjax(err.message,'search.js');
      }
    }
    else {return resolve(emptyresult);}
  });

  return new Promise((resolve, reject) => {
    Promise.all([pSearchGBV, pSearchGoogle, pSearchTextGrid]).then(values => {
      var res = [...values[0], ...values[1], ...values[2]];
      console.log(res);
      resultsCounter = res.length;
      $("span#numberOfResults").text('count: '+resultsCounter);
      resolve(res);
    })
  });
}

function errorAjax(err,source){
  var msg;
  if (source == 'GBV') {
    msg = "API error for source GBV: Not available"
  }
  else if (source == 'Google') {
    msg = "API error for source Google: Not available"
  }
  else if (source == 'search.js') {
    msg = "API error for source Google: Not available"
  }
  else if (source == 'TextGrid') {
    msg = "API error for source TextGrid: Not available"
  }
  displayError(msg);
}

function displayError(msg) {
  console.log('error: '+msg);
  displayLoaderErr();
  changeLoaderText('Error: '+msg);
  sessionStorage.setItem('error',true);
}

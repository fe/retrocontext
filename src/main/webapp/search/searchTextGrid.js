var searchTextGrid;
var jsondataTG;
var counterTextGrid=0;

searchTextGrid = function(keyword) {
  var resultkey;
  filterAddon = "";
  filterPD = false;
  searchUndefinied = 'true';
  resultkey = "result";

  if (sessionStorage.getItem('publishedDate') == 'true') {
    filterPD = true;
    if (document.getElementById('someSwitchOptionPrimary').checked == false ) {
      searchUndefinied = 'false';
    }
  }
  if (sessionStorage.getItem('title') == 'true') {
    filterAddon = filterAddon + "%20AND%20(title:" + $("input#searchTitleContains").val() + ")";
  }
  if (sessionStorage.getItem('author') == 'true') {
    filterAddon = filterAddon + "%20AND%20(edition.agent.value:" + $("input#searchAuthorContains").val() + ")";
  }
  if (sessionStorage.getItem('langRestrict') == 'true') {
    var langRestrictVal = $("select#searchlangRestrict").val();
    if (langRestrictVal == 'English') {
      langRestrictVal = 'en';
      filterAddon = filterAddon + "%20AND%20(edition.language:" + langRestrictVal + ")";
    }
    else if (langRestrictVal == 'All') {
      /*Do Nothing*/
    }
    else if (langRestrictVal == 'German') {
      langRestrictVal = 'de';
      filterAddon = filterAddon + "%20AND%20(edition.language:" + langRestrictVal + ")";
    }
    else if (langRestrictVal == 'French') {
      langRestrictVal = 'fr';
      filterAddon = filterAddon + "%20AND%20(edition.language:" + langRestrictVal + ")";
    }
  }
  if (sessionStorage.getItem('textAvailability') == 'true') {
    var textAvailabilityVal = $("select#searchtextAvailability").val();
    if (textAvailabilityVal == 'Public full-text') {
      filterTA = "FULL_PUBLIC_DOMAIN";
    }
    else if (textAvailabilityVal == 'All') {
      filterTA = "";
    }
    else if (textAvailabilityVal == 'Sample preview') {
      filterTA = "SAMPLE";
    }
    else if (textAvailabilityVal == 'Under license') {
      filterTA = "license";
    }
  }
  /*
	PRINT TYPE NOT AVAILABLE FOR TEXTGRID
  }*/

  console.log('with filters: '+filterAddon);

  if (filterPD == true) {
    minPublishedDate = parseInt($("input#searchPublishedDateMin").val());
    maxPublishedDate = parseInt($("input#searchPublishedDateMax").val());
  }
  else {
    minPublishedDate = 0;
    maxPublishedDate = 2017;
  }

  return new Promise( resolve =>
    ajaxCallTextGrid(keyword,0).then( responseArr =>{
    
    $(responseArr).each(function(key, response) {
      var result = $(response).find('result');
	    $(result).each(function(key, value) {
	      var tg_author, tg_identifier, tg_linkToText, tg_pubDate, tg_title;

	      tg_identifier = $(value).find('textgridUri').text();
	      tg_title = $(value).find('title').text();

              if($(value).find('author').text().length > 0){
		      tg_author = $(value).find('author').text();
	      }else{
		tg_author = "undefined"
	      }
	      if($(value).find('dateOfPublication').text().length > 0){
	      	tg_pubDate = $(value).find('dateOfPublication').text();
	      }else{
		tg_pubDate = "undefined"
	      }

	      textAvailability = "FULL_PUBLIC_DOMAIN"

	      if ($(value).find('format').text() === "text/tg.edition+tg.aggregation+xml" || $(value).find('format').text() === "text/xml") {
            
		tg_linkToText = "https://textgridrep.org/browse/-/browse/" + tg_identifier.substring("textgrid:".length, tg_identifier.length).replace(".", "_");
	      }
              tg_metadata = "https://textgridlab.org/1.0/tgcrud-public/rest/" + tg_identifier + "/metadata"
	      parseResultElementTextGrid(tg_identifier, tg_title, tg_author, tg_pubDate, textAvailability, tg_identifier, tg_linkToText)
	    });
	});


      var jsondataTGBegin = "[";
      var jsondataTGEnd = "]";

      jsondataTG = jsondataTGBegin + jsondataTG + jsondataTGEnd;
	
      return resolve(jsondataTG);
    })
  );
};

ajaxCallTextGrid = function(keyword, start) {

return new Promise( resolve => {  

  $.ajax({
    type: 'GET',
    url: "https://textgridlab.org/1.0/tgsearch-public/search?q=" + keyword + filterAddon + "&start=" + start,
    cache: false,
    dataType: "xml",
    error: function (err) {
    	errorAjax(JSON.stringify(err),'TextGrid');
    },
  }).done(function(data) {

    response = $(data).find('response');
    var hits = parseInt(response.attr('hits'));
    var limit = parseInt(response.attr('limit'));
    var resultsstart = parseInt(response.attr('start'));

      if((resultsstart+limit) < hits) {
        ajaxCallTextGrid(keyword,(resultsstart+limit)).then( result => {
          result.push(response);
          resolve(result);
        })
      } else {
          //Abbruchbedingung
 	var res = [response];
          	resolve(res);	 
      }
  });
});
};


function parseResultElementTextGrid(id, title, author, publishedDate, textAvailability, sourceid, url) {
  var newtitle="";
  var newauthor="";

  if (publishedDate == "0") {
    publishedDate = "n.d.";
  }
  if ((title != "") && (typeof title !== "undefined")){
    newtitle = title.replace(/[\"]/g, "'");
  }
  else {
    newtitle = "undefined";
  }
  if ((author != "") && (typeof author !== "undefined")){
    newauthor = author.replace(/[\"]/g, "'");
  }
  else{
    newauthor = "undefined";
  }

  if (counterTextGrid == 0) {
    jsondataTG = "{" +
      "\"id\":" + counterTextGrid + "," +
      "\"title\":" + "\"" + newtitle + "\"" +  "," +
      "\"author\":" + "\"" + newauthor + "\"" + "," +
      "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
      "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
      "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
      "\"source\":" + "\"" + "TextGrid" + "\" " + "," +
      "\"url\":" + "\"" + url + "\" " +
      "}";
  }
  else {
    jsondataTGTemp = ",{" +
      "\"id\":" + counterTextGrid + "," +
      "\"title\":" + "\"" + newtitle + "\"" +  "," +
      "\"author\":" + "\"" + newauthor + "\"" + "," +
      "\"publishedDate\":" + "\"" + publishedDate + "\"" + "," +
      "\"textAvailability\":" + "\"" + textAvailability + "\"" + "," +
      "\"sourceid\":" + "\"" + sourceid + "\"" + "," +
      "\"source\":" + "\"" + "TextGrid" + "\" " + "," +
      "\"url\":" + "\"" + url + "\" " +
      "}";
    jsondataTG = jsondataTG + jsondataTGTemp;
  }
  counterTextGrid++;
}
